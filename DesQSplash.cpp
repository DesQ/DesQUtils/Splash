/*
 *
 * Copyright 2021 Britanicus <marcusbritanicus@gmail.com>
 *
 *
 *
 * This file is a part of DesQ project ( https://gitlab.com/desq/ )
 * DesQSplash provides a simple splash screen during log in and log out.
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "DesQSplash.hpp"
#include <desq/Utils.hpp>

DesQSplash::DesQSplash( Mode mode ) : QWidget() {
    mSplashMode = mode;
    desq        = QIcon::fromTheme( "desq" ).pixmap( 256 );

    setFixedSize( qApp->primaryScreen()->size() );

    /* If the display manager is X11, we need to let it know that we are a SplashScreen */
    setAttribute( Qt::WA_X11NetWmWindowTypeSplash );

    /* No frame, and stay on top */
    Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint;
    setWindowFlags( wFlags );

    timer = new QBasicTimer();
    timer->start( 50, this );

    lineX = width() / 2;
    lineY = height() * 2.0 / 3.0;

    pixStart = QPoint( (width() - desq.width() ) / 2, (height() - desq.height() ) / 4 );

    /* By 10s everything will be complete */
    QTimer::singleShot( 10000, qApp, &QApplication::quit );

    text = "Wating for DesQ Shell...";
}


void DesQSplash::handleMessages( QString message ) {
    /* Close the SplashScreen */
    if ( message == "quit" ) {
        close();
    }

    else {
        text = QString( message );
    }

    qApp->processEvents();
    repaint();
}


void DesQSplash::close() {
    QWidget::close();
}


void DesQSplash::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == timer->timerId() ) {
        /* Flip */
        if ( grow and delta >= 20 ) {
            grow = false;
        }

        if ( not grow and delta <= 1 ) {
            grow = true;
        }

        delta += (grow ? 1 : -1);

        repaint();
    }

    QWidget::timerEvent( tEvent );
}


void DesQSplash::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( QColor( 0xF8, 0xDE, 0x7E ) );
    painter.drawRect( rect() );
    painter.restore();

    painter.drawPixmap( QRect( pixStart, desq.size() ), desq );

    painter.save();
    painter.setPen( QPen( QColor( 0x00, 0xA6, 0x93, 180 ), 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
    painter.drawLine( QPoint( lineX - delta, lineY ), QPoint( lineX + delta, lineY ) );
    painter.restore();

    painter.save();
    painter.setPen( QPen( QColor( 0x58, 0x11, 0x1A, 180 ), 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
    painter.setFont( QFont( "Raleway", 12 ) );
    painter.drawText( QRect( 0, height() - 50, width(), 50 ), Qt::AlignCenter, text );
    painter.restore();

    painter.end();

    pEvent->accept();
}
