/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

// Local Headers
// #include "Global.hpp"
#include "DesQSplash.hpp"

// Wayland Headers
#include <wayqt/WlGlobal.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/Application.hpp>

int main( int argc, char **argv ) {
    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    WQt::Application app( "DesQSplash", argc, argv );

    if ( app.isRunning() ) {
        qDebug() << "App already running.";

        return 0;
    }

    QFontDatabase fdb;
    fdb.addApplicationFont( ":/fonts/Raleway-Regular.ttf" );

    /* strcmp(...) returns 0 when the two strings are equal */
    DesQSplash *splash = new DesQSplash( DesQSplash::LOGIN );

    QObject::connect( &app, &WQt::Application::messageReceived, splash, &DesQSplash::handleMessages );

    splash->show();

    /** This is exclusively for wayfire - requires wayfire dbusqt plugin */
    if ( WQt::isWayfire() ) {
        WQt::Registry *registry = qApp->waylandRegistry();

        WQt::LayerShell::LayerType lyr  = WQt::LayerShell::Overlay;
        WQt::LayerSurface          *cls = registry->layerShell()->getLayerSurface( splash->windowHandle(), nullptr, lyr, "de-widget" );

        cls->setAnchors(
            WQt::LayerSurface::Top |
            WQt::LayerSurface::Right |
            WQt::LayerSurface::Bottom |
            WQt::LayerSurface::Left
        );

        /** Size of our surface */
        cls->setSurfaceSize( splash->size() );

        /** No gap anywhere */
        cls->setMargins( QMargins( 0, 0, 0, 0 ) );

        /** Move this for panel or others */
        cls->setExclusiveZone( -1 );

        /** We may need keyboard interaction. */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Commit to our choices */
        cls->apply();
    }

    return app.exec();
}
